var gulp = require('gulp');
var sass = require('gulp-sass');
var concat= require('gulp-concat');
var minifyCSS = require('gulp-csso');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');
var rename = require("gulp-rename");
var browserSync = require('browser-sync').create();

gulp.task('css', function(){
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(rename({ suffix: '.min'}))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
    });
gulp.task('js', function(){
    return gulp.src('src/js/**/*.js')
        .pipe(concat('script.bundle.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(rename({ suffix: '.min'}))
        
});

gulp.task('default', function(){
    return gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
});

gulp.task('default', function(){
    return gulp.src('src/app.css')
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('dist'))
});

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    });
    gulp.watch("src/scss/*.scss", ['css']);
    gulp.watch("src/js/*.js", ['js']);
    gulp.watch("src/*.html").on('change', browserSync.reload);
    
});



gulp.task('default', ['serve','css', 'js'])
