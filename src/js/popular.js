$.ajax({
    type: 'GET',
    url: "https://newsapi.org/v2/top-headlines?sources=mtv-news&apiKey=a0f4206073104370a40da90063695530",
    success: function(data) {
        popular=[];
        listaP=[];
        popular.push(data.articles);
        for(article in popular[0]){
            var single = {}
            single['src'] = popular[0][article].urlToImage;
            single['url'] = popular[0][article].url;
            single['date'] = popular[0][article].publishedAt;
            single['text'] = popular[0][article].title;
            listaP.unshift(single);       
        }
        var i;
        for(i=0; i<5;){
            var figur = document.createElement("figure")
            var img = document.createElement("img");
            var naslov =document.createElement("figcaption")
            text = document.createTextNode(listaP[i].text)
            naslov.appendChild(text)
            img.src = listaP[i].src
            var fi = document.getElementById("popularbar");
            var a = document.createElement("a");
            date=listaP[i].date.split("T")[0];
            a.text=formatdate(date);
            figur.appendChild(img);
            figur.appendChild(a);
            figur.appendChild(naslov);
            fi.appendChild(figur);
            i++;
        }
    }
    
});


$.ajax({
    type: 'GET',
    url: "https://newsapi.org/v2/top-headlines?sources=financial-times&apiKey=a0f4206073104370a40da90063695530",
    success: function(data) {
        featured=[];
        listaF=[];
        featured.push(data.articles);
        for(article in featured[0]){
            var single = {}
            single['src'] = featured[0][article].urlToImage;
            single['url'] = featured[0][article].url;
            single['date'] = featured[0][article].publishedAt;
            single['text'] = featured[0][article].title;
            listaF.unshift(single);       
        }
        var i;
        for(i=0; i<2;){
            var figur = document.createElement("figure")
            var img = document.createElement("img");
            var naslov =document.createElement("figcaption")
            text = document.createTextNode(listaF[i].text)
            naslov.appendChild(text)
            img.src = listaF[i].src
            var fi = document.getElementById("Featured");
            var a = document.createElement("a");
            date=listaF[i].date.split("T")[0];
            a.text=formatdate(date);
            figur.appendChild(img);
            figur.appendChild(a)
            figur.appendChild(naslov)
            fi.appendChild(figur);
            i++; 
        }
        var figur = document.createElement("figure")
        var img = document.createElement("img");
        var naslov =document.createElement("figcaption")
        text = document.createTextNode(listaF[2].text)
        naslov.appendChild(text)
        img.src = listaF[2].src
        var fi = document.getElementById("Featured");
        var a = document.createElement("a");
        date=listaF[2].date.split("T")[0];
        a.text=formatdate(date);
        figur.setAttribute("style", "border-bottom:none")
        figur.appendChild(img);
        figur.appendChild(a);
        figur.appendChild(naslov);
        fi.appendChild(figur);
    }
    
});

$.ajax({
    type: 'GET',
    url: "https://newsapi.org/v2/top-headlines?sources=axios&apiKey=a0f4206073104370a40da90063695530",
    success: function(data) {
        random=[];
        listaR=[];
        random.push(data.articles);
        for(article in random[0]){
            var single = {}
            single['src'] = random[0][article].urlToImage;
            single['url'] = random[0][article].url;
            single['date'] = random[0][article].publishedAt;
            single['text'] = random[0][article].title;
            listaR.unshift(single);       
        }
        var i;
        for(i=0; i<2;){
            var figur = document.createElement("figure")
            var img = document.createElement("img");
            var naslov =document.createElement("figcaption")
            text = document.createTextNode(listaR[i].text)
            naslov.appendChild(text)
            img.src = listaR[i].src
            var fi = document.getElementById("Random");
            var a = document.createElement("a");
            date=listaR[i].date.split("T")[0];
            a.text=formatdate(date);
            figur.appendChild(img);
            figur.appendChild(a);
            figur.appendChild(naslov);
            fi.appendChild(figur);
            i++; 
        }
        var figur = document.createElement("figure")
        var img = document.createElement("img");
        var naslov =document.createElement("figcaption")
        text = document.createTextNode(listaR[2].text)
        naslov.appendChild(text)
        img.src = listaR[2].src
        var fi = document.getElementById("Random");
        var a = document.createElement("a");
        date=listaR[2].date.split("T")[0];
        a.text=formatdate(date);
        figur.setAttribute("style", "border-bottom:none")
        figur.appendChild(img);
        figur.appendChild(a);
        figur.appendChild(naslov);
        fi.appendChild(figur);
        i++; 
    }
    
});

